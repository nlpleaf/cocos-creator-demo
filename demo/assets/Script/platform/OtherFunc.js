// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
//require("Script/framwork/Utils/qrcode")
//require("Script/framwork/Utils/reqrcode")
//import reqrcode from "Script/framwork/Utils/reqrcode";
//require("Script/framwork/Utils/jquery.min")

function getObjectURL(file) {
    var url = null;
    if (window.createObjectURL!=undefined) {
        url = window.createObjectURL(file) ;
    } else if (window.URL!=undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file) ;
    } else if (window.webkitURL!=undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file) ;
    }
    return url ;
}

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    //H5获取系统图片
    uploadImg(callback){
        var fileInput = document.getElementById("fileInput");
        var mime = {'png': 'image/png', 'jpg': 'image/jpeg', 'jpeg': 'image/jpeg', 'bmp': 'image/bmp'};
        function tmpSelectFile1(evt) {
            //console.log("image selected...");
            var file = evt.target.files[0];
            var type = file.type;
            if (!type) {
                type = mime[file.name.match(/\.([^\.]+)$/i)[1]];
            }
            var url = getObjectURL(file);
            
            loadLocalimg2(url);    
        }

        function loadLocalimg2(uri)
        {
            //创建一个div   
            var my = document.createElement("div");   
            document.body.appendChild(my);   
            my.style.position="absolute";   
            my.id="divCreator";
            my.style.width=100;   
            my.style.height=100;   
            my.style.backgroundColor="#ffffcc";   
            my.innerHTML = '<img id=imghead>';
            var img = document.getElementById('imghead');
            img.onload = function(){
                let texture = new cc.Texture2D();
                    texture.initWithElement(img);
                    texture.handleLoadedTexture();
                console.log("callbackcallbackcallbackcallback",callback)
                callback(texture)
                img.remove()
                my.remove()
                fileInput.remove()
            }
            img.src = uri;
            my.style.display='none';
            my.style.visibility = "hidden";
        }
        
        fileInput = document.createElement("input");
        fileInput.id = "fileInput";
        fileInput.type = "file";
        fileInput.accept = "image/*";
        fileInput.style.height = "0px";
        fileInput.style.display = "block";
        fileInput.style.overflow = "hidden";
        document.body.insertBefore(fileInput,document.body.firstChild);
        fileInput.addEventListener('change', tmpSelectFile1 , false);
        setTimeout(function(){fileInput.click()},100);
    },

    getTexture () {
        let node = new cc.Node();
        node.parent = cc.director.getScene().getChildByName('Canvas');
        let camera = node.addComponent(cc.Camera);
        //设置相机参数
       
        camera.enabled = false; // 避免自动渲染
        // 截图的缩放比例       
        let zoom = 1;
        let width = cc.winSize.width;  
        let height = cc.winSize.height; 
        let size = cc.size(width*zoom, height*zoom);

        // 截图的中心点就是摄像机节点的位置
        let origin = cc.v2(0, 0);
        
        camera.zoomRatio = zoom; // 设置缩放

        // 设置目标渲染纹理
        let texture = new cc.RenderTexture();
        texture.initWithSize(size.width, size.height);  // 截图矩形的尺寸
        this.node.setPosition(origin);                  // 截图矩形的中心点

        camera.targetTexture = texture;

        // 缓存，备用
        this._camera = camera;
        this._texture = texture;
        
        //用于显示的sprite组件，如果要测试这个，需要添加sprite组件
        this._sprite = this.getComponent<cc.Sprite>(cc.Sprite);
    },

    saveTexture(){
        console.log('save');
        
       // 执行一次 render，将所渲染的内容渲染到纹理上
       this._camera.render(undefined);
       // 到这里，截图就已经完成了

        // 接下去，可以从 RenderTexture 中获取数据，进行深加工
        let texture = this._texture;
        let data = texture.readPixels();

        let width = texture.width;
        let height = texture.height;

        // 接下来就可以对这些数据进行操作了       
        // let canvas:HTMLCanvasElement;
        let canvas = document.createElement('canvas'); 
        // document.body.appendChild(btn); // 没有添加到body上，不用担心内存泄漏

        let ctx = canvas.getContext('2d');
        canvas.width = width;
        canvas.height = height;

        // 1维数组转2维
        // 同时做个上下翻转
        let rowBytes = width * 4;
        for (let row = 0; row < height; row++) {
            let srow = height - 1 - row;
            let imageData = ctx.createImageData(width, 1);
            let start = srow*width*4;
            for (let i = 0; i < rowBytes; i++) {
                imageData.data[i] = data[start+i];
            }

            ctx.putImageData(imageData, 0, row);
        }

        let dataUrl = canvas.toDataURL("image/jpeg");

        //把图片生成后download到本地
        
        var href = dataUrl.replace(/^data:image[^;]*/, "data:image/octet-stream");
        //document.location.href = href;

        var a = document.createElement('a');          // 创建一个a节点插入的document
        a.href = href; 
        a.download = 'asd.png';
        a.click();
        a.remove();
    },

    //H5扫描二维码
    scanQRCode(callback){
        var fileInput = document.createElement("input");
        function tmpSelectFile(evt) {
            var file = evt.target.files[0];
            var url = getObjectURL(file);
            qrcode.decode(url); 
            qrcode.callback = function(imgMsg){
                callback(imgMsg)
                fileInput.remove()
            }
        }

        fileInput.id = "fileInput";
        fileInput.type = "file";
        fileInput.accept = "image/*";
        fileInput.style.height = "0px";
        fileInput.style.display = "block";
        fileInput.style.overflow = "hidden";
        document.body.insertBefore(fileInput,document.body.firstChild);
        fileInput.addEventListener('change', tmpSelectFile , false);
        setTimeout(function(){fileInput.click()},100);
    },

    createQRcode(code,callback){
        var doc_qrcode = document.createElement("img");
        doc_qrcode.id = "qrcode";
        document.body.insertBefore(doc_qrcode,document.body.firstChild);
        var qrcode1 = new QRCode(doc_qrcode, {
            width : 100,
            height : 100
            });

        qrcode1.makeCode(code);

        var scan = $("img[alt='Scan me!']")[0]
        LOG.INFO("scan",scan)
        scan.onload = function(){
            LOG.INFO("导入中scan",scan)
            let texture = new cc.Texture2D();
            texture.initWithElement(scan);
            texture.handleLoadedTexture();
            callback(texture)
            doc_qrcode.remove()
            scan.remove()
        }
    },

    //H5录音
    audio(callback){
        function tmpSelectFile(evt) {           
            var url = getObjectURL(file);
            qrcode.decode(url); 
            qrcode.callback = function(imgMsg){
                callback(imgMsg)
            }
        }

        var audioInput = document.createElement("audio");
        audioInput.id = "audioInput";
        audioInput.type = "audio";
        audioInput.accept = "audio/*";
        document.body.insertBefore(audioInput,document.body.firstChild);
    },
    //暂时不支持ios系统
    vibratez(times){
        if (navigator.vibrate) {
            //vibrate 1 second
            navigator.vibrate(times);
        } else if (navigator.webkitVibrate) {
            navigator.webkitVibrate(times);
        }
        else if (navigator.mozVibrate) {
            navigator.mozVibrate(times);
        }
        else if (navigator.msVibrate) {
            navigator.msVibrate(times);
        }
    },

    //复制到剪贴板
    copyborad(str){
        var textArea = document.getElementById("clipBoard");
        if (textArea === null) {
            textArea = document.createElement("textarea");
            textArea.id = "clipBoard";
            textArea.textContent = str;
            document.body.appendChild(textArea);
        }
        textArea.select();
        try {
            const msg = document.execCommand('copy') ? 'successful' : 'unsuccessful';
            document.body.removeChild(textArea);
        } catch (err) {
            promptText("复制失败")
        }
    }
});

