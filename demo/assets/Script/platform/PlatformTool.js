//分平台功能接口
var iosclass = require("IOSFunc")
var apkclass = require("AndroidFunc")
var otherclass = require("OtherFunc")
var ios = new iosclass()
var apk = new apkclass()
var other = new otherclass()

var PlatformTool = cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    //获取版本号
    getVersion(){
		LOG.INFO("检查已安装的分享类型",shareTextName)
        if (CC_JSB || cc.sys.isNative){
    		if(cc.sys.os == cc.sys.OS_ANDROID){ 
                apk.getVersionToJava()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                ios.getVersionToOc()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
	},

	//检查已安装的分享或登录类型
	getExitShareByName(shareTextName){
		LOG.INFO("检查已安装的分享类型",shareTextName)
		if (shareTextName == "mobile") {
			return true
		}
		else{			
			if (shareTextName == "wx" ){ 
				return this.isInstallWeixin()
			}
			else if(shareTextName == "qq"){
				return this.isInstallQq()
			}
		}		
	},
    
    loginWx(callback){
    	window.loginCallback = callback
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                apk.callWeixinLoginToJava()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                ios.callWeixinLoginToOc()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    //分享
    shareWx(title,content,imagePath,url,shareType,sessionOrFriendCircle,callback){
    	window.shareCallback = callback
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                apk.callWeixinShareToJava(title,content,imagePath,url,shareType,sessionOrFriendCircle)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                ios.callWeixinShareToOc(title,content,imagePath,url,shareType,sessionOrFriendCircle)
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },
   

    //检查是否安装微信
    isInstallWeixin(){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.isInstallWeixin()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.isInstallWeixin()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    loginQQ(callback){
    	window.loginCallback = callback
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                apk.callQqLoginToJava()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                ios.callQqLoginToOc()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    //分享qq
    shareQQ(title,content,imagePath,url,shareType,sessionOrFriendCircle,callback){
    	window.shareCallback = callback
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                apk.callQqShareToJava(title,content,imagePath,url,shareType,sessionOrFriendCircle)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                ios.callQqShareToOc(title,content,imagePath,url,shareType,sessionOrFriendCircle)
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    //检查是否安装QQ
    isInstallQq(){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.isInstallQq()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.isInstallQq()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    //开始录音
    startRecorder(fileName){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.callStartRecorderToJava(fileName)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.callStartRecorderToOc(fileName)
            }
        }
        else{
            //other.audio(fileName)
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    //停止录音
    stopRecorder(){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.callStopRecorderToJava()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.callStopRecorderToOc()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    cancelRecorder(){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.callCancelRecorderToJava()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.callCancelRecorderToOc()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    playRecorder(fileName){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.callPlayRecorderToJava(fileName)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.callPlayRecorderToOc(fileName)
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    stopPlayRecorder(){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.callStopPlayRecorderToJava()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.callStopPlayRecorderToOc()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    setStorageDir(dir){
        if (CC_JSB || cc.sys.isNative){
            let filePath = jsb.fileUtils.getWritablePath() + "/"+dir
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.callSetStorageDirToJava(filePath)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.callSetStorageDirToOc(filePath)
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    getBatteryTo(){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.getBatteryToJava()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.getBatteryToOc()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
            return 0
        }
    },

    //获取wifi的信号强度
    getWifiSignal(){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.getWifiSignalToJava()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.getWifiSignalToOc()
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
            return 0
        }
    },    

    copyborad(str){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                apk.copyborad(str)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                ios.copyborad(str)
            }
        }
        else{
            other.copyborad(str)
        }
    },

    openUrl(url){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.openUrl(url)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.openUrl(url)
            }
        }
        else{
            window.open(url)
        }
    },

    //调用震动
    vibratez(aSecond){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                apk.vibratez(aSecond)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                ios.vibratez(aSecond)
            }
        }
        else{
            console.log("navigator",navigator)
            other.vibratez(aSecond)
        }
    },

    //横屏竖屏转换
    setScreenDirection(direct){
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.setScreenDirection(direct)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.setScreenDirection(direct)
            }
        }
        else{
            console.log("platform:" + cc.sys.os + " dosn't implement share.");
        }
    },

    //扫描二维码
    scanQRCode(callback){
    	window.scamCallback = callback
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.scanQRCode()
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.scanQRCode()
            }
        }
        else{
            other.scanQRCode(scamCallback)
        }
    },

    createQRcode(code,callback){
    	window.createQRcodeCallback = callback
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.createQRcode(code,callback)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.createQRcode(code,callback)
            }
        }
        else{
            other.createQRcode(code,callback)
        }  
    },
    //上传图片
    uploadImg(callback){
        console.log("上传图片uploadImg(callback)")
    	window.uploadImgCallbcak = callback
        if (CC_JSB || cc.sys.isNative){
        	if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.uploadImg(callback)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.uploadImg(callback)
            }
        }
        else{
            other.uploadImg(callback)
        }    	
    },
    //上传头像
    uploadTx(callback){
        console.log("上传头像uploadTx(callback)")
        window.uploadImgCallbcak = callback
        if (CC_JSB || cc.sys.isNative){
            if(cc.sys.os == cc.sys.OS_ANDROID){ 
                return apk.uploadTx(callback)
            }
            else if(cc.sys.os == cc.sys.OS_IOS){
                return ios.uploadTx(callback)
            }
        }
        else{
            other.uploadImg(callback)
        }       
    },

    //针对节点截屏
    captureTextureScreen(callback,_node) {
        //注意，EditBox，VideoPlayer，Webview 等控件无法截
        if (CC_JSB || cc.sys.isNative) {
            
            let node = new cc.Node();
            node.parent = _node;
            node.width = _node.width;
            node.height = _node.height;
            console.log()
            //注意了，这里要讲节点的位置改为右上角，不然截图只有1/4
            node.x = node.width/2;
            node.y = node.height/2;
     
            let camera = node.addComponent(cc.Camera);
            // 设置你想要的截图内容的 cullingMask
            camera.cullingMask = 0xffffffff;
            
            // 新建一个 RenderTexture，并且设置 camera 的 targetTexture 为新建的 RenderTexture，这样 camera 的内容将会渲染到新建的 RenderTexture 中。
            let texture = new cc.RenderTexture();
            texture.initWithSize(node.width, node.height,cc.gfx.RB_FMT_S8);
            camera.targetTexture = texture;     
            camera.render();
            let data = texture.readPixels();
            let width = texture.width;
            let height = texture.height;
            let texture1 = new cc.RenderTexture();
            // 截图默认是y轴反转的，渲染前需要将图像倒过来，渲染完倒回去
            var picData = this.filpYImage(data,width,height)
            texture1.initWithData(picData, 32, width, height)

            let fileName = getDateMd5()+".jpg";
            let filePath = jsb.fileUtils.getWritablePath() + fileName;

            let success = jsb.saveImageData(picData,width,height, filePath)
            callback(filePath)
        }
        else{

        }
    },

    filpYImage (data, width, height) {
        // create the data array
        let picData = new Uint8Array(width * height * 4);
        let rowBytes = width * 4;
        for (let row = 0; row < height; row++) {
            let srow = height - 1 - row;
            let start = srow * width * 4;
            let reStart = row * width * 4;
            // save the piexls data
            for (let i = 0; i < rowBytes; i++) {
                picData[reStart + i] = data[start + i];
            }
        }
        return picData;
    },

    //全屏截图
    captureScreen(callback) {
        //注意，EditBox，VideoPlayer，Webview 等控件无法截
        if (CC_JSB || cc.sys.isNative) {            
            let node = new cc.Node();
            node.parent = cc.director.getScene();
            node.width = cc.visibleRect.width;
            node.height = cc.visibleRect.height;
            console.log()
            //注意了，这里要讲节点的位置改为右上角，不然截图只有1/4
            node.x = node.width/2;
            node.y = node.height/2;
     
            let camera = node.addComponent(cc.Camera);
            // 设置你想要的截图内容的 cullingMask
            camera.cullingMask = 0xffffffff;
            
            // 新建一个 RenderTexture，并且设置 camera 的 targetTexture 为新建的 RenderTexture，这样 camera 的内容将会渲染到新建的 RenderTexture 中。
            let texture = new cc.RenderTexture();
            texture.initWithSize(node.width, node.height,cc.gfx.RB_FMT_S8);
            camera.targetTexture = texture;     
            camera.render();
            let data = texture.readPixels();
            let width = texture.width;
            let height = texture.height;
            let texture1 = new cc.RenderTexture();
            // 截图默认是y轴反转的，渲染前需要将图像倒过来，渲染完倒回去
            var picData = this.filpYImage(data,width,height)
            texture1.initWithData(picData, 32, width, height)

            let fileName = getDateMd5()+".jpg";
            let filePath = jsb.fileUtils.getWritablePath() + fileName;

            let success = jsb.saveImageData(picData,width,height, filePath)
            callback(filePath)
        }
        else{
            //var capture = require("Script/framwork/Utils/capture/niuniucapture.js")
            console.log("进入截屏")
            //// 确保在浏览器中
            // 监听director绘制完成的事件
            cc.director.once(cc.Director.EVENT_AFTER_DRAW, function(){
                // 获取base64截图
                const canvas = document.getElementById("GameCanvas");
                //document.body.insertBefore(canvas,document.body.firstChild);
                const base64 = canvas.toDataURL();
                    
                // 创建一个http的img元素
                const img = new Image();
                img.src = base64;
         
                // 1.用cc.Sprite组件显示截图
                img.onload = function(){
                    const texture = new cc.Texture2D();
                    texture.initWithElement(img);
                    texture.handleLoadedTexture();
                    callback(texture)
                    console.log("截图信息")
                }
                //将图片下载
                var href = base64.replace(/^data:image[^;]*/, "data:image/octet-stream");
                //document.location.href = href;

                var a = document.createElement('a');          // 创建一个a节点插入的document
                a.href = href; 
                a.download = getDateMd5()+'.png';
                a.click();
                a.remove();
            })
        }
    },

    changeOrientation (direct) {
        if (CC_JSB || cc.sys.isNative){
            var succ = this.setScreenDirection(direct)
            if(!succ){
                promptText("切换屏幕失败")
                return
            }
        }
        else{
            var frameSize = cc.view.getFrameSize()  
            var node = cc.find('Canvas')
            var cvs = node.getComponent(cc.Canvas)
            var dw = cvs.designResolution.width
            var dh = cvs.designResolution.height

            if(direct == 1)//竖屏
            {
                cc.view.setOrientation(cc.macro.ORIENTATION_PORTRAIT)
                if(frameSize.width > frameSize.height)      
                {  
                    cc.view.setFrameSize(frameSize.height,frameSize.width); 
                    //cc.Canvas.instance.designResolution = cc.size(dw,dh) 
                }
            }
            else{
                cc.view.setOrientation(cc.macro.ORIENTATION_LANDSCAPE)
                if(frameSize.height > frameSize.width){    
                    cc.view.setFrameSize(frameSize.height,frameSize.width);
                    //cc.Canvas.instance.designResolution = cc.size(dh,dw) 
                } 
            } 
        }    
    },

    
})

window.PlatformTool = new PlatformTool();


//分享系统回调接口
window.shareCallback = function(data){
	promptText("分享回调:"+data)
}

//登录系统回调接口
window.loginCallback = function(data){
	promptText("登录回调:"+data)
}

//扫描回调
window.scamCallback = function(data){
	promptText("扫描回调:"+data)
}

window.createQRcodeCallback = function(data){
	promptText("二维码回调:"+data)
}

window.uploadImgCallbcak = function(texture){

}

window.statusBarOrientationChanged = function(){
    var frameSize = cc.view.getFrameSize()  

    if(direct == 1)//竖屏
    {
        cc.view.setOrientation(cc.macro.ORIENTATION_PORTRAIT)
        if(frameSize.width > frameSize.height)      
        {  
            cc.view.setFrameSize(frameSize.height,frameSize.width); 
        }
    }
    else{
        cc.view.setOrientation(cc.macro.ORIENTATION_LANDSCAPE)
        if(frameSize.height > frameSize.width){    
            cc.view.setFrameSize(frameSize.height,frameSize.width);
        } 
    } 

}

// --旋转效果  1 为横屏 2为竖屏
// function PlatformTool:setScreenDirection(direct)
// 	local hall_data = ACT.HALL.data
// 	hall_data.direct = hall_data.direct or 2
// 	if hall_data.direct ~= direct then
// 		hall_data.direct = direct
// 		if targetPlatform == cc.PLATFORM_OS_ANDROID then
// 			instance:setScreenDirection(direct)
// 		elseif targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
// 			instance:setScreenDirection(direct)
// 		end
// 		local pEGLView = cc.Director:getInstance()
// 		local size = display.sizeInPixels
// 		local glview = pEGLView:getOpenGLView()
// 		glview:setFrameSize( size.height,size.width)
// 		--设定设计分辨率
// 		CONFIG_SCREEN_AUTOSCALE = "FIXED_HEIGHT"
// 		local height = CONFIG_SCREEN_HEIGHT
// 		CONFIG_SCREEN_HEIGHT  = CONFIG_SCREEN_WIDTH
// 		CONFIG_SCREEN_WIDTH = height
// 		glview:setDesignResolutionSize(CONFIG_SCREEN_WIDTH,CONFIG_SCREEN_WIDTH, 1)
// 		_RESET_DISPLAY_CLASS()
// 		local pos = cc.p(GSM.scene:getPosition())
// 		GSM.scene:setPosition(1,1)
// 		GSM.scene:setPosition(pos)
// 	end
// end

// function PlatformTool:setScreenDirectionPlay(direct)
// 	local hall_data = ACT.HALL.data
// 	hall_data.direct = hall_data.direct or 2
// 	if hall_data.direct ~= direct then
// 		hall_data.direct = direct
// 		if targetPlatform == cc.PLATFORM_OS_ANDROID then
// 			instance:setScreenDirection(direct)
// 		elseif targetPlatform == cc.PLATFORM_OS_IPHONE or targetPlatform == cc.PLATFORM_OS_IPAD then
// 			instance:setScreenDirection(direct)
// 		end

// 		if device.platform == "windows" then
// 			local pEGLView = cc.Director:getInstance()
// 			--LOG.USERDATA(pEGLView,"pEGLView")

// 			local size = display.sizeInPixels
// 			local glview = pEGLView:getOpenGLView()
// 			glview:setFrameSize( size.height,size.width)
// 			--设定设计分辨率
// 			CONFIG_SCREEN_AUTOSCALE = "FIXED_HEIGHT"
// 			local height = CONFIG_SCREEN_HEIGHT
// 			CONFIG_SCREEN_HEIGHT  = CONFIG_SCREEN_WIDTH
// 			CONFIG_SCREEN_WIDTH = height
// 			_RESET_DISPLAY_CLASS()
// 		end
// 	end
// end


// function _RESET_DISPLAY_CLASS()
// 	-- 记录下当前已经加载的文件
//     for k, _ in pairs(package.loaded) do
//     	if string.find(k,"hall.src.framework.cc.net.SimpleTCP") then
//     	elseif k == "hall.src.app.extends.panel" then
//             package.loaded[k] = false
//         elseif k == cc.PACKAGE_NAME .. ".cc.init" then
//         	package.loaded[k] = false
//         elseif string.find(k,cc.PACKAGE_NAME) ~= nil then
//         	package.loaded[k] = false
//         elseif string.find(k,"hall.src.cocos") ~= nil then
//         	package.loaded[k] = false
//        	end
//     end
//     require("hall.src.cocos.init")
//     require(cc.PACKAGE_NAME .. ".init")
//     require("hall.src.app.extends.panel")
// end


