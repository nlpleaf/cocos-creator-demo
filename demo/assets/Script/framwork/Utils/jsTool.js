//与js有关的工具类都在此实现

var getType = function(obj){
   //tostring会返回对应不同的标签的构造函数
   var toString = Object.prototype.toString;
   var map = {
      '[object Boolean]'  : 'boolean', 
      '[object Number]'   : 'number', 
      '[object String]'   : 'string', 
      '[object Function]' : 'function', 
      '[object Array]'    : 'array', 
      '[object Date]'     : 'date', 
      '[object RegExp]'   : 'regExp', 
      '[object Undefined]': 'undefined',
      '[object Null]'     : 'null', 
      '[object Object]'   : 'object'
  };
  if(obj instanceof Element) {
       return 'element';
  }
  return map[toString.call(obj)];
}
window.getType = getType;

var DeepCopy = function(data){
   var type = getType(data);
   var obj;
   if(type === 'array'){
       obj = [];
   } else if(type === 'object'){
       obj = {};
   } else {
       //不再具有下一层次
       return data;
   }
   if(type === 'array'){
       for(var i = 0, len = data.length; i < len; i++){
           obj.push(DeepCopy(data[i]));
       }
   } else if(type === 'object'){
       for(var key in data){
           obj[key] = DeepCopy(data[key]);
       }
   }
   return obj;
 }
 window.DeepCopy = DeepCopy;


window.promptText = function(str,pos){
  var spritenode = new cc.Node();
  var sprite = spritenode.addComponent(cc.Sprite);
  //var widget = spritenode.addComponent(cc.Widget);
  //sprite.spriteFrame = cc.textureCache.addImage("resources/Img/logo/notifyUI_bg.png") //new cc.SpriteFrame("Img/logo/shop_price_bg.png");
  spritenode.x = 750/2
  spritenode.y = 1334/2-15
  cc.director.getScene().addChild(spritenode);

  var labelnode = new cc.Node();
  var label = labelnode.addComponent(cc.Label);
  label._string = str
  label._fontSize = 30
  label._lineHeight = 30
  // label.node.height = label.node.height*0.75
  spritenode.addChild(labelnode);
  cc.loader.loadRes("/Img/logo/shop_price_bg.png",cc.SpriteFrame,function(err,spriteFarme){
        //LOG.INFO("spriteFarme",spriteFarme)
        sprite.spriteFrame = spriteFarme;
        sprite.node.width = label.node.width+30
        sprite.node.height = label.node.height
      })
  var seqAction = cc.sequence(cc.moveBy(1,0,30),cc.removeSelf())
  spritenode.runAction(seqAction)
  cc.game.addPersistRootNode(spritenode)
}


window.getDate = function(){
  var date = new Date;
  var year = date.getFullYear();
  var month = date.getMonth();
  var day = date.getDate();
  var hou = date.getHours() 
  var min = date.getMinutes()
  var seconds = date.getSeconds()
  return cc.js.formatStr("%s-%s-%s %s:%s", year, month + 1,day,hou,min);
}


window.setTouxiang = function(dikuang , FaceUrl , callback){
  var spritenode = new cc.Node();
  var sprite = spritenode.addComponent(cc.Sprite);
  dikuang.addChild(spritenode);
  var k_w = dikuang.width
  var k_h = dikuang.height
  LOG.INFO("dikuang",dikuang)
  cc.loader.loadRes(FaceUrl,cc.SpriteFrame,function(err,spriteFarme){
      //LOG.INFO("spriteFarme",spriteFarme)
      sprite.spriteFrame = spriteFarme;
      var s_w = spritenode.width
      var s_h = spritenode.height
      LOG.INFO("sprite",sprite)
      spritenode.scaleX = k_w/s_w
      spritenode.scaleY = k_h/s_h
    })

  return spritenode
}

window.getDateMd5 = function(){
  var myDate = new Date;
  return CryptoJS.MD5(myDate.toLocaleTimeString()).toString();
}