/* 
管理层的类，随着场景结束而结束
主要负责他们之间互相调用时注册相应的回调函数
*/ 

window.actLayer = cc.Class({
    extends: cc.Component,

    properties: {
        list:[],//已经加载的层
    },

    //加载一个层
    addPreFabLayer:function(flag,layer,zLndex,callback){
        self = this;
        self.list[flag] = [];

        if(typeof(zLndex) != "number"){
            zLndex = 0
        }
        cc.loader.loadRes(layer, function (err, prefab) {
            var newNode = cc.instantiate(prefab);
            cc.director.getScene().addChild(newNode,zLndex);
            self.list[flag]['layer'] = newNode;
            self.list[flag]['control'] = newNode.getComponent(flag)
            if(typeof(callback) == 'function'){
                callback()
            }
        });   
    },

    //取得一个已加载的层
    getLayer:function(flag){
        return self.list[flag]
    },

    //移除一个已加载的层
    removeLayer:function(layerControl){
        var flag = layerControl.layerFlag;
        if(this.list[flag] && this.list[flag]['layer']){
            this.list[flag]['layer'].destroy()
            this.list[flag]['layer'] = null
        }
    },

    //注册层之间的内部调用
    registCall:function(layerControl){
        var flag = layerControl.layerFlag;
        if(this.list[flag]) 
            this.list[flag]['control'] = layerControl;
        else
            LOG.INFO("注册回调失败");
    },

    //相互之前通讯调用
    sendCall:function(flag,fun,calldata){
        LOG.INFO(this.list)
        if(this.list[flag]
            && this.list[flag]['control']
            && this.list[flag]['control'][fun]){
            this.list[flag]['control'][fun](calldata);
        }
        else{
            LOG.INFO("未找到通讯回调失败");
        }        
    },

    //销毁内部调用
    distoryCall:function(layerControl){
        var flag = layerControl.layerFlag;
        this.list[flag] = null;
    },

    //移除当前所有层
    removeAllLayer:function(){
        for( var idx in this.list){
            if(this.list[idx] && this.list[idx]['layer']){
                this.list[idx]['layer'].destroy()
                this.list[idx]['layer'] = null
            }
        }
    },

});
