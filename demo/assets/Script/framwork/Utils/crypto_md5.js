// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

var crypto_md5 = cc.Class({
    extends: cc.Component,

    properties: {
        crypto_pwd:'123456',
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },
    //密码校验字符拼接
    cryptoStr:function(datastr,key){
        var str = this.crypto_pwd+'datastr'+datastr+'key'+key+this.crypto_pwd;
        return str;
    },


    //加密 {data={},mod5='abcd',key=12345}
    encode:function(data,key){
        var enstr = this.cryptoStr(data,key);
        var session = CryptoJS.MD5(enstr).toString();
        return session;
    },

    //解密
    decode:function(data,key){
        var _key = data.key;
        var _session = data.session;
        var _data = data.data;
        if(_key != key){
            console.log("解密key不相同");
            return false;
        }

        var destr = this.cryptoStr(_data,key);
        var session = CryptoJS.MD5(destr).toString();
        if(_session != session){
            console.log("加密session不相同");
            return false;
        }
        return true;
    },
    // update (dt) {},
});

window.crypto_md5 = new crypto_md5();
