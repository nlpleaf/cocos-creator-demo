// 用户基础数据
/*
Uid 
Account 
Gender
MobilePhone
ID_card_no
RealName 
NickName
FaceUrl
FaceID
CustomID
is_real
sign
login_type
RoomCardNum
GoldNum
Coin
IP
SKey     
exp_value
next_level_exp
level
*/

//用户数据类
cc.Class({
    extends: cc.Component,

    properties: {

        userData:[],

    },

    ctor:function(...params){
        this.userData = params[0];
        window.userSession = this.userData.userSession;
        var userStorage = new StorageData(window.appkey,this.userData['Uid'],window.version);
        userStorage.setJsonByKey(this.userData);
        this.userStorage = userStorage;
    },

    //设置用户数据
    setUserData:function(ukey,value){
        this.userData[ukey] = value;
    },

    //取出用户数据
    getUserData:function(ukey){
        return this.userData[ukey]?this.userData[ukey]:'';
    },

    //存储登录基础数据
    saveUserData:function(ukey){
        this.userStorage.setDataBykey(ukey,this.getUserData(ukey));
    },

    //读取历史基础数据
    readUserData:function(ukey){
        this.userStorage.setDataBykey(ukey,this.getUserData(ukey));
    },

    // update (dt) {},
});
