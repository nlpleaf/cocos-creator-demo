// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
//let CryptoJS = require("CryptoJS");

var StorageData = cc.Class({
    extends: cc.Component,

    properties: {
        appkey:'',//软件秘钥
        userkey:'',//用户秘钥
        version:'',//当前版本
    },



    //创建一个数据模块实例
    ctor:function(appkey,userkey,version){
        this.appkey = ""+appkey;//软件秘钥
        this.userkey = ""+userkey;//用户秘钥
        this.version = ""+version;//当前版本
    },

    enKey:function(key){
        var jskeys = CryptoJS.MD5((this.appkey+this.userkey+key).toString(CryptoJS.enc.Utf8));
        LOG.INFO("jskeys",jskeys)
        var enkeys = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(jskeys));
        LOG.INFO("enkeys",enkeys)
        return enkeys;
    },


    enData:function(data){
        LOG.INFO("data",data)
        LOG.INFO("this.appkey:"+this.appkey);
        if(typeof(data) != "string"){
            return ""
        }
        var jsData = CryptoJS.AES.encrypt((""+data).toString(CryptoJS.enc.Utf8),this.appkey+this.userkey);
        var enData =  CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(jsData));
        return enData
    },

    
    //给数据解密
    deData:function(data){
        LOG.INFO("data",data)
        if(typeof(data) != "string"){
            return ""
        }
        var jsData = CryptoJS.enc.Base64.parse(""+data).toString(CryptoJS.enc.Utf8);
        var decData = CryptoJS.AES.decrypt(jsData,this.appkey+this.userkey).toString(CryptoJS.enc.Utf8);
        LOG.INFO("decData",decData)
        return decData;
    },

    //存储数据
    setDataBykey:function(key,datas){
        var get_en_key = this.enKey(key);
        var get_en_data = this.enData(datas);
        cc.sys.localStorage.setItem(get_en_key,get_en_data);
    }, 

    //取出数据
    getDataBykey:function(key){
        var get_en_key = this.enKey(key);
        var dn_datas = cc.sys.localStorage.getItem(get_en_key);
        var datas = this.deData(dn_datas);
        return datas;
    },  
    
    //删除数据
    removeDataBykey:function(key){
        var get_en_key = this.enKey(key);
        cc.sys.localStorage.removeItem(get_key);
    },

    //写入，修改int数据
    setIntByKey:function(key,intvalue){
        this.setDataBykey(key,intvalue);
    },
    
    //读取int数据
    getIntByKey:function(key){
        var intvalue = this.getDataBykey(key);
        return (0+intvalue);
    },

    //删除int数据
    removeIntByKey:function(key){
        this.removeDataBykey(key);
    },
    
    //写入，修改bool数据
    setBoolByKey:function(key,boolvalue){
        var boolstr = boolvalue?'true':'false';
        this.setDataBykey(key,boolstr);
    },
    
    //读取bool数据
    getBoolByKey:function(key){        
        var boolstr = this.getDataBykey(key);
        var boolvalue = boolstr=='true'?true:false;
    },

    //删除bool数据
    removeBoolByKey:function(key){        
        this.removeDataBykey(key);
    },
    
    //设置string数据
    setStringBykey:function(key,str){
        if(typeof(str) != 'string'){
            str = ''
        }
        this.setDataBykey(key,str);
    }, 

    //取出string数据
    getStringBykey:function(key){
        var str = this.getDataBykey(key);
        if(typeof(str) != 'string'){
            str = ''
        }
        return str;
    },  
    
    //删除string数据
    removeStringBykey:function(key){
        this.removeDataBykey(key);
    },
    
    //将表转换为json
    setJsonByKey:function(key,jsonData){
        var jsonObj = JSON.stringify(jsonData);
        this.setDataBykey(key,jsonObj);
    },

    //获取json数据并转换为表
    getJsonByKey:function(key){
        LOG.INFO("key:"+key);
        var jsonStr = this.getDataBykey(key);
        LOG.INFO("jsonStr:"+jsonStr);
        var jsonObj = [];
        LOG.INFO("typeof(jsonStr)="+typeof(jsonStr));
        LOG.INFO("jsonStr.length="+jsonStr.length);
        if(typeof(jsonStr) == 'string' && jsonStr.length >= 2){
            jsonObj = JSON.parse(jsonStr);
        }
        LOG.INFO("jsonObj:");
        LOG.INFO(jsonObj);
        return jsonObj;
    },

    //删除一条json数据
    removeJsonByKey:function(key){
        this.removeDataBykey(key);
    },   

    //给对应key的数据json增加一条数据
    setJsonInByKey:function(key,jsonkey,jsonData){
        var jsonStr = this.getDataBykey(key);
        var jsonObj = JSON.parse(jsonStr);
        jsonObj[jsonkey] = jsonData;
        var jsonAddStr = JSON.stringify(jsonObj);
        this.setDataBykey(key,jsonData);
    },

    //获取对应key的json数据
    getJsonInByKey:function(){
        var jsonStr = this.getDataBykey(key);
        var jsonObj = JSON.parse(jsonStr);
        return ''+jsonObj[jsonkey];
    },

    //给对应key的数据json删除一条数据
    removeJsonInByKey:function(key,jsonkey){
        var jsonStr = this.getDataBykey(key);
        var jsonObj = JSON.parse(jsonStr);
        jsonObj[jsonkey] = null;
        var jsonRemoveStr = JSON.stringify(jsonObj);
        this.setDataBykey(key,jsonData);
    },   
});

window.StorageData = StorageData;