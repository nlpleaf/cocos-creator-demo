// 登录数据
/*
Uid 
Account 
Gender
MobilePhone
ID_card_no
RealName 
NickName
FaceUrl
FaceID
CustomID
is_real
sign
login_type
RoomCardNum
GoldNum
Coin
IP
SKey     
exp_value
next_level_exp
level
*/

//用户数据类
window.loginData = cc.Class({
    extends: cc.Component,

    properties: {

        loginData:[],//所有用户的登录数据
        loginSessoin:'',//本地登录临时授权码
    },

    ctor:function(){
        //初始化登录数据管理表
        var loginStorage = new StorageData(window.appkey,'',window.version);
        this.loginStorage = loginStorage;
        LOG.INFO('this.loginStorage');
        LOG.INFO(this.loginStorage);
        //初始化本地临时授权码
        this.loginSessoin = this.getLoginSessoin()
        LOG.INFO(this.loginSessoin);
        //未发现历史临时会话重新生成一个临时会话
        if(typeof(this.loginSessoin) != 'string' 
            || this.loginSessoin.length <= 0){
            LOG.INFO("未发现历史临时会话重新生成一个临时会话");
            this.loginSessoin = this.getLoginSessoinByDate();
        }
        this.setLoginSessoin(this.loginSessoin);//存储临时会话
        LOG.INFO(CryptoJS);
        this.getLoginSessoinByDate();

        //初始化loginData
        this.loginData = this.getLoginData(); 
    },

    //设置登录数据
    setLoginData:function(value){
        this.loginStorage.setJsonByKey('loginData',value);
    },

    //取出用户登录数据
    getLoginData:function(){
        return this.loginStorage.getJsonByKey('loginData');
    },

    //设置一个用户登录数据
    setLoginaData:function(lKey,value){
        this.loginData[lKey] = value;
        this.loginStorage.setJsonInByKey('loginData',lKey,JSON.stringify(value));
    },

    //取出一个用户登录数据
    getLoginaData:function(ukey){
        var user = this.loginStorage.getJsonInByKey('loginData',lKey);
        return user
    },

    //设置当前登陆数据只有手机号和密码
    setLoginNewData:function(data){
        this.loginStorage.setJsonByKey("loginNewData",data)
    },

    //取出最后登陆账号的登陆数据
    getLoginNewData:function(data){
        var newData = this.loginStorage.getJsonByKey("loginNewData")
        return newData
    },

    //清除当前登陆数据
    deleteLoginNewData:function(){
        this.loginStorage.setStringBykey("loginNewData","")
    },

    //设置临时授权码
    setLoginSessoin:function(value){
        this.loginStorage.setStringBykey('loginSessoin',value);
    },

    //取出临时授权码
    getLoginSessoin:function(){
        return this.loginStorage.getStringBykey('loginSessoin');;
    },

    //设置用户临时授权码
    setUserSessoin:function(value){
        this.loginStorage.setStringBykey('userSessoin',value);
    },

    //取出用户临时授权码
    getUserSessoin:function(){
        return this.loginStorage.getStringBykey('userSessoin');
    },

    //根据本机当前时间获取临时会话
    getLoginSessoinByDate:function(){
        var myDate = new Date;
        var loginSessoin = CryptoJS.MD5(myDate.toLocaleTimeString()).toString();
        LOG.INFO("loginSessoin",loginSessoin);
        return loginSessoin;
    },

    // update (dt) {},
});
