// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        day_item : cc.Node,
        record_1 : cc.Node,
        record_2 : cc.Node,
        record_3 : cc.Node,
        record_4 : cc.Node,
        day_num  : cc.Label,
        showColor : cc.color(0,0,0),
        flagColor: cc.color(0,200,0),
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.record = [
            {record : this.record_1,data : "",stage : ""},
            {record : this.record_2,data : "",stage : ""},
            {record : this.record_3,data : "",stage : ""},
            {record : this.record_4,data : "",stage : ""},
        ]
    },

    // start () {
        
    // },

    //初始
    init(){
        for(var i = 0;i < this.record.length ; i++){
            this.record[i].record.active = false
        }
        //this.setDate("")
    },

    //设置日期
    setDate(date){
        //var label = this.day_num.getComponent(cc.Label)
        this.day_num.string = date
    },

    //标记日期为选中日期
    setDateFlag(b){
        if(b)
            this.day_num.node.color = this.flagColor
        else
            this.day_num.node.color = this.showColor
    },

    //设置节点的显示与不显示
    setActive(b){
        this.day_item.active = b
    },

    //设置一个标记状态
    setRecordState(record,data,stage){
        LOG.INFO("record,data,stage",record,data,stage)
        var recordinfo = this.record[record];
        LOG.INFO("this.record",this.record)
        LOG.INFO("recordinfo",recordinfo)
        recordinfo.data = data
        recordinfo.stage = stage
        recordinfo.record.active = true
    },

    btnClick:function(event, customEventData){
        this.callback(event,{day:this.day_num.string,info:this.record})
    },

    callback(){

    },
    
    //显示当前事务信息
    

    // update (dt) {},
});
