cc.Class({
    extends: cc.Component,

    properties: {
        // itemTemplate: { // item template to instantiate other items
        //     default: null,
        //     type: cc.Node
        // },
        scrollView: {
            default: null,
            type: cc.ScrollView
        },
        totalCount: 0, // how many items we need for the whole list
        spacing: 5, // space between each item
        bufferZone: 600, // when item is away from bufferZone, we relocate it
    },

    // use this for initialization
    onLoad: function () {
        this.items = []; // array to store spawned items
        this.content = this.scrollView.content;
        this.bar = this.scrollView.bar;
        //this.hideBar()
        this.updateTimer = 0;
        this.updateInterval = 0.2;
        this.lastContentPosY = 0; // use this variable to detect if we are scrolling up or down
    },

    //取得滑动框的总元素
    getItemsLenth:function(){
        return this.items.length;
    },

    //隐藏bar
    hideBar:function(){
        var bar = this.scrollView.bar;
        var scrollBar = this.scrollView.scrollBar;
        scrollBar.width = 0
        bar.width = 0
    },

    //计算总偏移量
    calculateOffset:function(){
        let offset = 0;
        let items = this.items;
        for (let i = 0; i < items.length; ++i){
            let item = items[i];
            offset = offset + this.spacing + item.height;
        }
        return offset;
    },

    upadteItemList: function(){
        var items = this.items;
        var hegiht = 0;
        for (let i = 0; i < items.length; ++i){
            var item = items[i];  
            hegiht = hegiht -  item.height  - this.spacing;
            item.setPosition(0, hegiht + item.height/2 );
        }  
        this.content.height = Math.abs(hegiht)
    },

    addItem: function(_item,itempos) { 
        this.content.addChild(_item);
        var items = this.items;
        if(typeof(itempos) == 'number'){
            itempos = itempos>items.length?items.length:itempos
        }
        else{
            itempos = items.length + 1
        }
        this.items.splice(itempos,0,_item);
        this.totalCount = this.totalCount + 1;  
        this.upadteItemList()  
    },

    addYItem: function(_item,itempos) { 
        var items = this.items;
        if(typeof(itempos) == 'number'){
            itempos = itempos>items.length?items.length:itempos
        }
        else{
            itempos = items.length + 1
        }
        this.items.splice(itempos,0,_item);
        this.totalCount = this.totalCount + 1;  
        this.upadteItemList()  
    },

    removeItem: function(itempos) {
        if(typeof(itempos) != 'number'){
            return            
        }
        if(itempos > this.totalCount)
        {
            return
        }
        var items = this.items;
        var removeItems = items.splice(itempos,1);
        var removeItem = removeItems[0]
        
        this.content.height = this.content.height -  (removeItem.height + this.spacing); // get total content height
        this.totalCount = this.totalCount - 1;
        removeItem.destroy()
        this.upadteItemList()  
    },

    removeAllItem: function(){
        this.content.removeAllChildren()
        this.items = []
        this.content.height = 0
        this.totalCount = 0
    },

    //移动到指定元素位置
    scrollToItmePos:function(itmepos){
        if(typeof(itempos) != 'number'){
            return            
        }
        if(itempos > this.totalCount)
        {
            itempos = this.totalCount
        }
        if(itempos < 0)
        {
            itempos = 0
        }

        var moveItem = this.items[itempos];
        this.scrollToFixedPosition(cc.v2(0,moveItem.node.x))
    },

    //移动到头部
    scrollToTop:function(){
        var moveItem = this.items[0];
        this.scrollToFixedPosition(cc.v2(0,moveItem.node.x))
    },

    //移动到底部
    scrollToBottom:function(){
        var moveItem = this.items[0];
        this.scrollToFixedPosition(cc.v2(0,moveItem.node.x))
    },

    //移动到对应位置
    scrollToFixedPosition: function (pos) {//cc.v2(0, 500)
        this.scrollView.scrollToOffset(pos, 2);
    },

    scrollEvent: function(sender, event,cenvet) {
        switch(event) {
            case 0: 
               //this.lblScrollEvent.string = "Scroll to Top"; 
               break;
            case 1: 
               //this.lblScrollEvent.string = "Scroll to Bottom"; 
               break;
            case 2: 
               //this.lblScrollEvent.string = "Scroll to Left"; 
               break;
            case 3: 
               //this.lblScrollEvent.string = "Scroll to Right"; 
               break;
            case 4: 
               //this.lblScrollEvent.string = "Scrolling"; 
               break;
            case 5: 
               //this.lblScrollEvent.string = "Bounce Top"; 
               break;
            case 6: 
               //this.lblScrollEvent.string = "Bounce bottom"; 
               break;
            case 7: 
               //this.lblScrollEvent.string = "Bounce left"; 
               break;
            case 8: 
               //this.lblScrollEvent.string = "Bounce right"; 
               break;
            case 9: 
               //this.lblScrollEvent.string = "Auto scroll ended"; 
               break;
        }
    },

});
