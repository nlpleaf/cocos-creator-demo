// 日历表

cc.Class({
    extends: cc.Component,

    properties: {
        day_item : cc.Node,
        day_list : cc.Node,
        date_txt : cc.Label,

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.items = []   //所有日历节点
        this.itemdays = [] //当前显示的日历节点
        this.initDayItems()
        var date = new Date;
        this.year = date.getFullYear();
        this.mon = date.getMonth()+1;
        this.day = date.getDate();
        this.showDate(this.year,this.mon,this.day)
    },

    //初始日历天数的及位置
    initDayItems(){
        var add_x = 100
        var add_y = -100
        var init_x = 75
        var init_y = -550
        
        for(var y = 5;y >= 0; y--){
            for(var x = 0;x <= 6 ; x ++){
                var item = cc.instantiate(this.day_item);
                item.x = init_x + x*add_x
                item.y= init_y + y*add_x
                this.day_list.addChild(item)
                var itemCtr = item.getComponent('DayItem')
                itemCtr.setActive(false)
                this.items.push(itemCtr)
            }
        }
        LOG.INFO("this.items",this.items)
        //LOG.INFO("this.day_list",this.day_list)
    },

    //根据年月显示获得信息
    getRangDay(year,mon){
        var is_run = 0
        if(year%100 == 0){
            if(year % 4){
                is_run = 1
            }
        }
        else if(year % 4 == 0)
        {
            is_run = 1
        }
        switch(mon){
            case 1: return 31
            case 2: return is_run==1?29:28
            case 3: return 31
            case 4: return 30
            case 5: return 31
            case 6: return 30
            case 7: return 31
            case 8: return 31
            case 9: return 30
            case 10: return 31
            case 11: return 30
            case 12: return 31
        }
    },

    //根据年月日获得星期
    getWeek(data){
        var week = new Date(data).getDay();//注意此处必须是先new一个Date
        return week //[0,1,2,3,4,5,6]零是星期天
    },

    //根据年月显示当前日历
    showDate(year,mon,day){
        var rangday = this.getRangDay(year,mon)
        var week = this.getWeek(year+"-"+mon+"-"+"01")
        var end_i = week+rangday
        this.itemdays = []
        var date = 1
        for(var i = 0;i < this.items.length; i ++){
            var itemCtr = this.items[i]
            itemCtr.init()
            if(i >= week && i < end_i){
                this.itemdays[date] = itemCtr
                itemCtr.setActive(true)
                itemCtr.setDate(date+"")
                LOG.INFO("day == date",day == date)
                itemCtr.setDateFlag(day == date)
                itemCtr.callback = function(e,dateinfo){
                    LOG.INFO("dateinfo",dateinfo)
                    dateinfo.year = this.year
                    dateinfo.mon  = this.mon
                    this.chiceDate(dateinfo)
                }.bind(this)
                date = date + 1
            }
            else{
                itemCtr.setActive(false)
            }
        }
    },

    //选择日期回调
    chiceDate(){

    },

    //月份变化回调
    changeMon(){

    },

    btnClick:function(event, customEventData){
        var node = event.target;
        var button = node.getComponent(cc.Button);

        if(customEventData == 'date_chice_btn'){
            LOG.INFO("选择日期")
            msgView.chiceDateYm(function(date){
                this.year = date.year
                var changmon = this.mon != date.mon
                this.mon = date.mon
                this.date_txt.string = this.year+"年"+this.mon+"月"
                this.showDate(this.year,this.mon)
                if(changmon)
                    this.changeMon(this.year,this.mon)
                else
                    this.updateDateRecord(this.dataRecord)
            }.bind(this),function(){},{year:this.year,mon:this.mon})
        }
        else if(customEventData == 'add_mon_btn'){
            this.mon = this.mon + 1
            if(this.mon > 12){
                this.mon = 1
                this.year = this.year + 1
            }
            
            this.date_txt.string = this.year+"年"+this.mon+"月"
            this.showDate(this.year,this.mon)
            this.changeMon(this.year,this.mon)
        }
        else if(customEventData == 'del_mon_btn'){
            this.mon = this.mon - 1
            if(this.mon < 1){
                this.mon = 12
                this.year = this.year - 1
            }            
            this.date_txt.string = this.year+"年"+this.mon+"月"
            this.showDate(this.year,this.mon)
            this.changeMon(this.year,this.mon)
        }
    },

    //刷新日历数据
    updateDateRecord(dataRecord){
        this.dataRecord = dataRecord
        for( var i in dataRecord){
            LOG.INFO("iiiii",i)
            var record = dataRecord[i]
            LOG.INFO("record",record)
            var dateCtr = this.itemdays[i]
            for(var k = 0; k < record.length ; k ++){
                dateCtr.setRecordState(record[k].record,
                                        record[k].data,
                                        record[k].stage)
            }
        }
    },

    // update (dt) {},
});
