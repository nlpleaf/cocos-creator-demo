cc.Class({
    extends: cc.Component,
 
    properties: {
        curNum: 3,
        curTotal: 10,
        target: cc.PageView,
    },
 
    onLoad () {
        // 设置的当前页面为 1
        this.target.setCurrentPageIndex(0);
        //this.target.node.activeInHierarchy = false
    },

    setEnabled(b){
        this.target.enabled = b
    },  


    //移动到指定页
    onJumpIndex(idx) {
        this.target.scrollToPage(idx);
    },
 
    
    // 返回首页
    onJumpHome () {
        // 第二个参数为滚动所需时间，默认值为 0.3 秒
        this.target.scrollToPage(0);
    },
 
 
    // 添加页面
    onAddPage (apage) {
        if (this.curNum > this.curTotal) {
            return;
        }
        apage.x = 0
        apage.y = 0
        this.curNum ++;
        this.target.addPage(apage);
    },
 
    // 插入当前页面
    onInsertPage (apage) {
        if (this.curNum > this.curTotal) {
            return;
        }
        apage.x = 0
        apage.y = 0
        this.curNum ++;
        this.target.insertPage(apage, this.target.getCurrentPageIndex());
    },
 
    // 移除最后一个页面
    onRemovePage () {
        if (this.curNum <= 0) {
            return;
        }
        this.curNum--;
        var pages = this.target.getPages();
        this.target.removePage(pages[pages.length - 1]);
    },
 
    // 移除当前页面
    onRemovePageAtIndex () {
        if (this.curNum <= 0) {
            return;
        }
        this.curNum--;
        this.target.removePageAtIndex(this.target.getCurrentPageIndex());
    },
 
    // 移除所有页面
    onRemoveAllPage () {
        this.target.removeAllPages();
        this.curNum = 0;
    },
 
    // 监听事件
    onPageEvent (sender, eventType,cevent) {
        LOG.INFO("sender",sender)
        LOG.INFO("eventType",eventType)
        LOG.INFO("cevent",cevent)
        // 翻页事件
        if (eventType !== cc.PageView.EventType.PAGE_TURNING) {
            return;
        }
    },

    //添加触摸事件
    onAddTouch(){
        //监听触摸开始事件
        var x = 0
        var y = 0
        this.target.enabled = false

        this.node.on(cc.Node.EventType.TOUCH_START,function(t){
            //函数体内写事件发生时的事情
            //当触摸开始是打印以下字样
            var n_pos = t.getLocation();
            x = n_pos.x
            y = n_pos.y
            //console.log(n_pos,n_pos.x,n_pos.y);
            console.log("触摸开始");
        },this);
        //监听触摸移动事件
        //使用自定义回调函数
        this.node.on(cc.Node.EventType.TOUCH_MOVE,function(t){
            var n_pos = t.getLocation();
            //打印触摸点的坐标，x坐标，y坐标
            //console.log(n_pos,n_pos.x,n_pos.y);
            var _x = n_pos.x
            var _y = n_pos.y
            if(Math.abs(_x-x) >= 20){
                this.target.enabled = true
            }
            else{
                this.target.enabled = false
            }
            //this.target.enabled = false
        },this);
        
        //监听作用域内触摸抬起事件
        this.node.on(cc.Node.EventType.TOUCH_ENDED,function(t){
            console.log("触摸内结束");
            this.target.enabled = false
        },this);
        // //监听作用域外触摸抬起事件
        // this.target.node.on(cc.Node.EventType.TOUCH_CANCEL,function(t){
        //     this.target.enabled = false
        //     console.log("触摸外开始");
        // },this);
    },

});
 
