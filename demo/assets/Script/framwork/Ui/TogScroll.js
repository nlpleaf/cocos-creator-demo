// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        togScroll : cc.Node,
        content   : cc.Node,  
        totalCount: 0, // how many items we need for the whole list
        spacing   : 5, // space between each item 
        showCount : 4,
        showColor : cc.color(0,0,0),
        choieColor: cc.color(0,200,0),
        item      : cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.cententx = 0
        this.cententy = 0
        this.width = this.togScroll.width
        this.items = []
        this.id = 0
        this.onAddTouch() 
        LOG.INFO("this.content",this.content)
        LOG.INFO("初始化togScroll onLoad")
    },

    start () {       
        
        LOG.INFO("初始化togScroll start")
    },

    init(item){
        this.item = item;
        this.item_width = item.width
        this.item_height = item.height
    },

    upadteItemList: function(){
        var height = 0
        var next_y = 0
        for(var i = 0 ; i < this.items.length ; i++){
            var item = this.items[i]
            item.color = this.showColor
            var add_height = (item.height + this.spacing)      
            height += add_height
            next_y = -(height - add_height/2)
            item.x = 0
            item.y = next_y
        }

        this.content.height = height
        this.content.width = this.width
        //定位到当前选择点
        this.setChiceById(this.id)
    },

    //添加
    addItem(_item,itempos){
        this.content.addChild(_item)
        var items = this.items  
        if(typeof(itempos) == 'number'){
            itempos = itempos>items.length?items.length:itempos
        }
        else{
            itempos = items.length+1
        }
        this.items.splice(itempos,0,_item);
        this.totalCount ++ 
        this.upadteItemList()
    },

    //移除
    removeItem(itempos){
        if(typeof(itempos) == 'number'){
            itempos = itempos>items.length?items.length:itempos
        }
        else{
            itempos = items.length
        }        
        var items = this.items;
        var removeItems = items.splice(itempos,1);
        var removeItem = removeItems[0]
        
        this.content.height = this.content.height -  (removeItem.height + this.spacing); // get total content height
        this.totalCount --;
        removeItem.destroy() 
        this.upadteItemList()
    },

    removeItemBykey(){
        for(var i = 0; i < this.items.length ; i++){
            var item = this.items[i]
            var label = item.getComponent(cc.Label)
            if(label.string == key){
                this.removeItem(i)
                return
            }
        }
    },

    removeAllItem(){
        this.content.height = 0
        this.totalCount = 0
        this.items = []
        this.content.removeAllChildren()        
    },


    //根据下标标设置当前选择点
    setChiceById(id){
        var item = this.items[id];
        if(typeof(item) == "null" || typeof(item) == "undefined" ){
            LOG.INFO("下标不存在")
            return
        }
        for(var i = id-this.showCount-2; i < id+this.showCount+2 ; i++){
            var _item = this.items[i]
            if(typeof(_item) == "null" || typeof(_item) == "undefined"){
                continue
            }
            _item.scale = 1
            _item.color = this.showColor
            var dis_abs = Math.abs(i-id)
            if(dis_abs > this.showCount){
                _item.opacity = 0
            }
            else if(dis_abs == 0){
                _item.opacity = 255
                _item.scale = 1.1
                _item.color = this.choieColor
            }
            else{
                _item.opacity = 255/dis_abs
            }
        }
        this.id = id
        this.content.y = -item.y
        var label = item.getComponent(cc.Label)
        this.chiceCall(label.string)
    },

    //选择好后的回调
    chiceCall(key){
        
    },

    //取得当前下标的key
    getchiceKey(){
        var item = this.items[this.id];
        var label = item.getComponent(cc.Label)
        return label.string
    },

    //根据key值去更新下标
    setChiceByKey(key){
        for(var i = 0; i < this.items.length ; i++){
            var item = this.items[i]
            var label = item.getComponent(cc.Label)
            if(label.string == key){
                this.setChiceById(i)
                return
            }
        }
    },
        
    //根据content当前位置查找一个离停止位最近的一个下标
    getItemId(){
        var content_y = this.content.y
        // var items_gap = this.item_height+this.spacing
        // var chu_id = content_y/items_gap
        // //LOG.INFO("content_y",content_y,"items_gap",items_gap,"chu_id",chu_id)
        // var id = 0
        // if(chu_id%1 > 0.5){
        //     id = Math.ceil(chu_id)
        // }
        // id = Math.floor(chu_id)
        // if(id > this.items.length-1){
        //     id = this.items.length-1
        // }
        // else if (id < 0){
        //     id = 0
        // }
        // return id
        var items = this.items
        var max_y = content_y
        var id = 0
        //取得一个离content_y最近的一个点
        for(var i = 0; i < items.length ; i++){
            var item = items[i]
            var abs_y = Math.abs(item.y+ content_y)
            if(max_y > abs_y){
                max_y = abs_y
                id = i
            }
        }
        return id
    },


    //根据下标标设置当前选择点
    updateChiceById(id){
  
        for(var i = id-this.showCount-2; i < id+this.showCount+2 ; i++){
            var _item = this.items[i]
            if(typeof(_item) == "null" || typeof(_item) == "undefined"){
                continue
            }
            _item.scale = 1
            _item.color = this.showColor
            var dis_abs = Math.abs(i-id)
            if(dis_abs > this.showCount){
                _item.opacity = 0
            }
            else if(dis_abs == 0){
                _item.opacity = 255
                _item.scale = 1.1
                _item.color = this.choieColor
            }
            else{
                _item.opacity = 255/dis_abs
            }
        }
    },
    
    //添加触摸事件
    onAddTouch(){
        //监听触摸开始事件
        var x = 0
        var y = 0

        this.togScroll.on(cc.Node.EventType.TOUCH_START,function(t){
            //函数体内写事件发生时的事情
            //当触摸开始是打印以下字样
            var n_pos = t.getLocation();
            x = n_pos.x
            y = n_pos.y
        },this);
        //监听触摸移动事件
        //使用自定义回调函数
        this.togScroll.on(cc.Node.EventType.TOUCH_MOVE,function(t){
            var n_pos = t.getLocation();
            //打印触摸点的坐标，x坐标，y坐标
            //console.log(n_pos,n_pos.x,n_pos.y);
            var _x = n_pos.x
            var _y = n_pos.y
            this.content.y += (_y-y)
            if(this.content.y <= 0){
                this.content.y = 0
            }
            else if(this.content.y >= this.content.height){
                this.content.y = this.content.height
            }
            y = n_pos.y
            this.move = true
        },this);
        
        //监听作用域内触摸抬起事件
        this.togScroll.on(cc.Node.EventType.TOUCH_END,function(t){
            this.setChiceById(this.getItemId())          
        },this);
        //监听作用域外触摸抬起事件
        this.togScroll.on(cc.Node.EventType.TOUCH_CANCEL,function(t){
        },this);
    },


    update (dt) {
        if(this.move){
            this.move = false
            this.updateChiceById(this.getItemId())
        }
    },
});
