//长连接封装接口

var Net = cc.Class({
    extends: cc.Component,

    properties: {
        ip:"",
        port:"",
        sio:null,
        isPinging:false,
        fnDisconnect:null,
        heart:0,
    },    

    //构造webSocket
    connect:function(_ip,_port,callfun){
        this.ip = _ip;
        this.port = _port;
        var webSocket = new WebSocket("ws://" + this.ip + ":" + this.port);
        webSocket.binaryType = 'arraybuffer';
        this.webSocket = webSocket;
        this.cOpen(function (event) {
            console.log("Send Text WS was opened.");
            if(typeof(callfun) == "function"){
                console.log("回调");
                callfun();
            }
        });
        this.cMessage(function (event) {
            console.log("response text msg: " + event.data);
        });
        this.cError(function (event) {
            console.log("Send Text fired an error");
        });
        this.cClose(function (event) {
            console.log("WebSocket instance closed.");
        });
        //this.startHearbeat();        
    },

    cOpen:function(callfun){
        var pthis = this;
        this.webSocket.onopen = function(evt){
            console.log("Send Text WS was opened.");
            pthis.startHearbeat();
            if(typeof(callfun) == "function")
                callfun(evt)
        };
    },

    cMessage:function(callfun) {
        this.webSocket.onmessage = function(evt){
            var result = String.fromCharCode.apply(null, new Uint8Array(evt.data));
            var ecode = decodeURIComponent(escape(result));
            var msgArray = JSON.parse(ecode.toString(CryptoJS.enc.Utf8));
            if(typeof(callfun) == "function")
                callfun(msgArray)
        };
    },

    cError:function(callfun) {
        this.webSocket.onerror = function(evt){
            console.log("Send Text fired an error");
            self.heartPackMsg = null;
            if(typeof(callfun) == "function")
                callfun(evt)
        };
    },

    cClose:function(callfun) {
        this.webSocket.onclose = function(evt){
            self.heartPackMsg = null;
            if(typeof(callfun) == "function")
                callfun(evt)
        };
    },

    //连接成功后的消息
    send:function(msg){   
        //LOG.INFO("msg",msg.toString(CryptoJS.enc.Utf8));    
        this.webSocket.send(msg.toString(CryptoJS.enc.Utf8));
    },

    close:function(){
        this.webSocket.close();
    },

    heartPack:function(){
        if(self.heartPackMsg){
            return self.heartPackMsg;
        }
        var jsonMsg = JSON.stringify({msgID:500,pack:{}});
        //LOG.INFO("jsonMsg:",jsonMsg);
        var key = CryptoJS.MD5(window.loginSession+window.userSession).toString();
        var session = crypto_md5.encode(jsonMsg,key);
        var data_session = {};
        data_session.data = jsonMsg;
        data_session.session = session;
        //LOG.INFO(data_session);
        self.heartPackMsg = JSON.stringify(data_session);
        return self.heartPackMsg;
    },

    //心跳
    startHearbeat:function(){
        console.log("启动心跳");
        //this.send(this.heartPack());
        this.schedule(function () {
            if (this.webSocket.readyState === WebSocket.OPEN) {
                this.send(this.heartPack());
            }
            else {
                console.log("WebSocket instance wasn't ready...");
            }
        }, 5);
    },
        
    ping:function(){
        this.send('game_ping');
    },
        
});

window.Net = Net;