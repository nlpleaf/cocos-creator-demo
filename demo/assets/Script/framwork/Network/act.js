// 系统消息管理控制列表

window.actNet = cc.Class({
    extends: cc.Component,

    properties: {
        msgCallList:[]//消息回调列表
    },

    //连接网络并注册对应的回调
    startNet:function(ip,port,callfun){
        LOG.INFO('初始化消息码列表');
        //初始化消息码列表
        this.initCallListByCmd();

        //随机码
        this.msgRemoveCode = 0;

        var net = new Net();
        // net.ip = ip + ":" + port
        LOG.INFO(net);
        net.connect(ip,port,function(data){
            if(typeof(callfun) == 'function')
                callfun(data)
        });

        this.net = net;
        //注册收到消息的回调
        net.cMessage(function(msg){
            this.protocolMsgCall(msg);
        }.bind(this))
    },

    //初始化消息码列表回调
    initCallListByCmd:function(){
        LOG.INFO("cmdNet",cmdNet);
        for( var cmd in cmdNet){
            LOG.INFO("cmd",cmd)            
            this.msgCallList[cmdNet[cmd]] = new Array()
            /*
            [_type   回调类型
                    1 调用后会被注销 
                    2.调用后继续保留 
                    3.必须要与回调码相同时才可调用，且只运行调用后删除
                    4.必须要与回调码相同时才可调用，且只运行调用后继续保留 
            _callfun 回调函数
            _checkcode 回调码,类型为3和4时需要比对此回调码才可调用对应的回调

            ]
             */
            //LOG.INFO("cmd:"+cmd);
        }
    },

    //取得随机删除码
    getRemoveCode:function(){
        this.msgRemoveCode = this.msgRemoveCode + 1;
        return CryptoJS.MD5(this.msgRemoveCode).toString();
    },

    //收到网络消息调用
    protocolMsgCall:function(msg){
        var msgID = msg.msgID;
        var pack = msg.pack;
        var checkcode = msg.checkcode;
        var delay = msg.delay;
        if(delay){
            LOG.INFO("网络延时");            
            return;
        }
        if(msgID == cmdNet.HEART_PACK){
            //LOG.INFO("心跳包不处理");            
            return;
        }
        //LOG.INFO("this.msgCallList",this.msgCallList)
        if(this.msgCallList[msgID] == null){
            LOG.INFO("消息码不存在")
            return;
        }
        var msgCall = this.msgCallList[msgID];
        var saveCall = new Array();
        for( var call of msgCall){
            var _type = call._type;
            var _callfun = call._callfun;
            var _checkcode = call._checkcode;
            switch(_type){
                case 1:{
                    _callfun(pack);
                    break; 
                }
                case 2:{
                    _callfun(pack);
                    saveCall.push(call);
                    break; 
                }
                case 3:{
                    if(_checkcode == checkcode){
                        _callfun(pack);
                    }
                    break; 
                }
                case 4:{
                    if(_checkcode == checkcode){
                        _callfun(pack);
                    }
                    saveCall.push(call);
                    break; 
                }
            }
        } 

        this.msgCallList[msgID] = saveCall;
    },
    
    //注册消息码及回调
    registMsgCall:function(msgID,_type,_callfun,_checkcode,timeout){        
        if(this.msgCallList[msgID] == null){
            LOG.INFO("消息码不存在,取消注册")
            return;
        }
        timeout = timeout?timeout:10;
        var _removeCode = this.getRemoveCode();
        var msgCall = this.msgCallList[msgID];
        msgCall.push({_type : _type,
                    _callfun : _callfun,
                    _checkcode : _checkcode,
                    _removeCode : _removeCode});
        //超时检查
        if(_type == 1 || _type == 3){
            this.schedule(function () {
                //显示网络延时的字样并开始重连
                this.cancelMsgCall(msgID,_removeCode);
            }, timeout);
        }
        return _removeCode;
    },
    
    //注销对应回调
    cancelMsgCall:function(msgID,removeCode){
        if(this.msgCallList[msgID] == null){
            LOG.INFO("消息码不存在,取消注册")
            return;
        }
        var saveCall = new Array();
        var msgCall = this.msgCallList[msgID];
        for( var call of msgCall){
            if(removeCode != call._removeCode){
                saveCall.push(call);
            }
        }
        this.msgCallList[msgID] = saveCall;
    },

    //发送消息
    sendMsg:function(msg,callfun,type,timeout){
        //LOG.INFO("msg",msg)
        var jsonMsg = JSON.stringify(msg)
        var key = CryptoJS.MD5(window.loginSession+window.userSession).toString()
        //执行加密操作
        var session = crypto_md5.encode(jsonMsg,key)
        var data_session = {}//data:data,key:key,session:session,{key:key},{session:session}];
        data_session.data = jsonMsg;
        data_session.key = key;
        data_session.session = session;

        var removeCode = '';
        if(typeof(callfun) == 'function'){
            var msgID = msg.msgID;
            type = type?type:1;//type默认1
            timeout = timeout?timeout:10;
            removeCode = this.registMsgCall(msgID,type,callfun,session,timeout);
        }
        var myMsg = JSON.stringify(data_session)
        //LOG.INFO('myMsg',myMsg);
        this.net.send(myMsg);
        return removeCode;
    },

    // update (dt) {},
});
