// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

window.cmdNet = {
    MSG_RESGIST_SESSION     : 100,          // 注册本机消息
    HEART_PACK              : 500,          // 心跳    
	//主包消息
    MOBILE_REGISTER         : 10001,       // 手机号注册
    RESERT_MOBILE_ACCOUNT   : 10002,       // 重设密码
    MOBILE_LOGIN            : 10003,       // 手机登录
};


window.msgPack = {
	MSG_RESGIST_SESSION:function(){
		return {msgID :cmdNet.MSG_RESGIST_SESSION , 
				pack:{loginSession:loginSession,
                      userSession:userSession}}
	},
	HEART_PACK:function(){
		return {msgID :cmdNet.HEART_PACK , pack:{}}
	},
	MOBILE_REGISTER:function(NickName,MobilePhone,PlatformPassword,check_num){
		return {msgID :cmdNet.MOBILE_REGISTER , 
				pack:{NickName:NickName,
					MobilePhone:MobilePhone,
					PlatformPassword:PlatformPassword,
					check_num:check_num,
					loginSession:loginSession,
				}}
	},
	RESERT_MOBILE_ACCOUNT:function(MobilePhone,PlatformPassword,check_num){
		return {msgID :cmdNet.RESERT_MOBILE_ACCOUNT , 
				pack:{MobilePhone:MobilePhone,
					PlatformPassword:PlatformPassword,
					check_num:check_num,
					loginSession:loginSession,
				}}
	},

	MOBILE_LOGIN:function(MobilePhone,PlatformPassword){
		return {msgID :cmdNet.MOBILE_LOGIN , 
				pack:{MobilePhone:MobilePhone,
					PlatformPassword:PlatformPassword,
					loginSession:loginSession,
				}}
	},

}