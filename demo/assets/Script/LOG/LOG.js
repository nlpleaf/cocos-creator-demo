// 写日志功能，项目标准日志
var LOG = cc.Class({
    // LIFE-CYCLE CALLBACKS:
    extends: cc.Component,

    properties: {
        fileName:'log.txt',
        
    },

    INFO:console.log,
    
    ERROR:function(s){
        this.saveAllLog("ERROR:"+s);
    },

    DUMP:function(s){
        this.saveAllLog("Object:"+s);
    },

    USERDATA:function(s){
        this.saveAllLog("USERDATA:"+s);
    },

    //保存到本地文件中  暂时未实现
    saveFile:function(s) {     
        //jsb.fileUtils.writeStringToFile(s,'c:/'+this.fileName)
    },

    //保存到服务器   暂时未实现
    saveServer:function(s) {

    },

    //写到日志中
    saveClog:function(s) {
        console.log(s);
    },

    //写到全部日志中控制器
    saveAllLog:function(s){
        this.saveClog(s);
        this.saveFile(s);
        this.saveServer(s);
    },

    getCallerArgument:function(){
        var result = [];
        var slice = Array.prototype.slice;
        var caller = arguments.callee.caller;

        while(caller){
            result = result.concat(slice.call(caller.arguments, 0));
            caller = caller.arguments.callee.caller;
        }
        return result;
    },

});


window.LOG = new LOG();