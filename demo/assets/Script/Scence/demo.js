// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
require ("../config")

cc.Class({
    extends: cc.Component,

    properties: {
        scroll: {
            default: null,
            type: cc.ScrollView
        },

        item        : cc.Node,
    },


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        window.demo = [];
        window.demo.Layer = new actLayer();
        this.call = [
            {name:"微信登录",data_type:1,calljs:function(callback){
                return PlatformTool.loginWx(callback)
            }},
            {name:"检查微信是否安装",data_type:1,calljs:function(){
                if(PlatformTool.isInstallWeixin()){
                    promptText("已安装")
                }
                else
                {
                    promptText("未安装")
                }
            }},
            {name:"微信文字分享给好友",data_type:1,calljs:function(callback){
                PlatformTool.shareWx("微信文字好友分享","文字分享内容","","http://www.yykjgame.com/",1,1,callback)
            }},
            {name:"微信文字分享到朋友圈",data_type:1,calljs:function(callback){
                PlatformTool.shareWx("微信文字朋友圈分享","文字分享内容","","http://www.yykjgame.com/",1,2,callback)
            }},
            {name:"微信图片分享给好友",data_type:1,calljs:function(callback){
                PlatformTool.captureScreen(function(path){
                    PlatformTool.shareWx("微信图片好友分享","图片分享内容",path,"",2,1,callback)
                })
            }},
            {name:"微信图片分享到朋友圈",data_type:1,calljs:function(callback){
                PlatformTool.captureScreen(function(path){
                    PlatformTool.shareWx("微信图片朋友圈分享","图片分享内容",path,"",2,2,callback)
                })
            }},
            {name:"QQ登录",data_type:1,calljs:function(callback){
                return PlatformTool.loginQQ(callback)
            }},
            {name:"检查QQ是否安装",data_type:1,calljs:function(){
                if(PlatformTool.isInstallQq()){
                    promptText("已安装")
                }
                else
                {
                    promptText("未安装")
                }
            }},
            {name:"QQ文字分享给好友",data_type:1,calljs:function(callback){
                PlatformTool.shareQQ("QQ文字好友分享","QQ分享内容","","http://www.yykjgame.com/",1,1,callback)
            }},
            {name:"QQ文字分享到朋友圈",data_type:1,calljs:function(callback){
                PlatformTool.shareQQ("QQ文字朋友圈分享","文字分享内容","","http://www.yykjgame.com/",1,2,callback)
            }},
            {name:"QQ图片分享给好友",data_type:1,calljs:function(callback){
                PlatformTool.captureScreen(function(path){
                    PlatformTool.shareQQ("QQ图片好友分享","图片分享内容",path,"",2,1,callback)
                })
            }},
            {name:"QQ图片分享到朋友圈",data_type:1,calljs:function(callback){
                PlatformTool.captureScreen(function(path){
                    PlatformTool.shareQQ("QQ图片朋友圈分享","图片分享内容",path,"",2,2,callback)
                })
            }},
            {name:"获取电量信息",data_type:1,calljs:function(){
                var battery = PlatformTool.getBatteryTo()
                promptText("当前电量"+battery)
            }},    
            {name:"获取wifi信号强度",data_type:1,calljs:function(){
                var WifiSignal = PlatformTool.getWifiSignal()
                promptText("当前wifi信号"+WifiSignal)
            }}, 
            {name:"打开网页",data_type:3,calljs:function(str){
                if(typeof(str) != "string" || str == "")
                    str = "http://www.yykjgame.com/"
                PlatformTool.openUrl(str)
            }},         
            {name:"复制文字",data_type:3,calljs:function(str){
                if(typeof(str) != "string" || str == "")
                    str = "http://www.yykjgame.com/"
                promptText("复制:"+str)
                PlatformTool.copyborad(str)
            }},
            {name:"手机震动",data_type:1,calljs:function(){
                PlatformTool.vibratez(1000)
            }},
            {name:"截取屏幕",data_type:2,calljs:function(callback){
                PlatformTool.captureScreen(callback)
            }},
            {name:"上传图片",data_type:2,calljs:function(callback){                
                PlatformTool.uploadImg(callback)
            }}, 
            {name:"上传头像",data_type:2,calljs:function(callback){                
                PlatformTool.uploadTx(callback)
            }}, 
            {name:"识别二维码",data_type:1,calljs:function(callback){
                PlatformTool.scanQRCode(callback)
            }},
            {name:"创建二维码",data_type:5,calljs:function(code,callback){
                if(typeof(code) != "string" || code == "")
                    code = "http://www.yykjgame.com/"     
                PlatformTool.createQRcode(code,callback)
            }}, 
            {name:"竖屏",data_type:1,calljs:function(){                
                PlatformTool.changeOrientation(1)
            }},  
            {name:"横屏",data_type:1,calljs:function(){                
                PlatformTool.changeOrientation(2)
            }},  
        ]

        PlatformTool.setStorageDir("voice")
        this.showCallType()        
    },

    showCallType(){
        var scroll = this.scroll.getComponent('scrollViewCtr')
        for(var i = 0; i < this.call.length; i ++){
            var config = this.call[i];
            var item = cc.instantiate(this.item)
            var calljsitem = item.getComponent("calljsItem")
            calljsitem.setCallByConfig(config)

            scroll.addItem(item)
        }
    },

    //所有按钮的回调
    btnClick:function(event, customEventData){
        if(customEventData == "group_title.btn"){
            //复制回调数据            
            promptText("复制:1140497703")
            PlatformTool.copyborad("1140497703")
        }
        else if(customEventData == "start_btn"){
            PlatformTool.startRecorder("voice")
        }
        else if(customEventData == "stop_btn"){
            PlatformTool.stopRecorder("voice")
        }
        else if(customEventData == "cancel_btn"){
            PlatformTool.cancelRecorder("voice")
        }
        else if(customEventData == "paly_btn"){
            PlatformTool.playRecorder("voice")
        }            
    },



    // update (dt) {},
});
