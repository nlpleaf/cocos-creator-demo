// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        fun_name    : cc.Label,
        calljs_data : cc.Label,
        fun_btn     : cc.Button,
        editbox     : cc.EditBox,
        uploadimg   : cc.Sprite,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.fun_name.node.active = false
        this.calljs_data.node.active = false
        this.editbox.node.active = false
        this.uploadimg.node.active = false
        if(this.config){
            console.log("this.config",this.config)
            this.setFunName(this.config.name)
            this.call = this.config.calljs;
            //this.setFunName(this.config.name)
            if(this.config.data_type >= 3){
                this.setEditbox()
            }
        }        
    },

    start () {

    },

    setCallByConfig(config){
        this.config = config
    },

    //所有按钮的回调
    btnClick:function(event, customEventData){
        var node = event.target
        var button = node.getComponent(cc.Button)
        if(customEventData == "fun_btn"){
            //console.log("this.config",this.config)
            this.funCall()
        }
        else if(customEventData == "calljs_data"){
            //复制回调数据
            var str = this.calljs_data.string
            promptText("复制:"+str)
            PlatformTool.copyborad(str)
        }
        else if(customEventData == "uploadImg"){
            //放大或缩小图片
            console.log("demo.Layer",demo.Layer)
            if (!(CC_JSB || cc.sys.isNative)){
                if(this.texture){
                    demo.Layer.addPreFabLayer('L_showimg','./L_showimg',100,function(){
                        demo.Layer.sendCall('L_showimg','setUpload',this.texture)
                    }.bind(this))
                    
                }
            }
        }
    },

    //功能回调
    funCall(){
        var returndata = ""
        if(this.config.data_type == 1)//有回调数据的处理
        {   
            console.log("this.config1",this.config)
            this.call(function(data){
                console.log("this.config",this.config)
                this.setCalljsData(data)
            }.bind(this))
        }
        else if(this.config.data_type == 2)//需要显示回调图片发处理
        {   
            console.log("this.config1",this.config)
            this.call(function(data){
                console.log("this.config",this.config)
                this.setUploadimg(data)
            }.bind(this))
        }
        else if(this.config.data_type == 3)//需要输入参数的处理
        {   
            this.call(this.editbox.string)
        }
        else if(this.config.data_type == 4)//需要输入参数和回调的处理
        {   
            this.call(this.editbox.string,function(data){
                this.setCalljsData(data)
            }.bind(this))
        }
        else if(this.config.data_type == 5)//需要输入参数和回调的处理
        {   
            this.call(this.editbox.string,function(data){
                this.setUploadimg(data)
            }.bind(this))
        }
    },

    //设置功能
    setFunName(funname){
        this.fun_name.node.active = true
        this.fun_name.string = funname
    }, 

    //设置编辑
    setEditbox(){
        this.editbox.node.active = true
    },

    //设置图片
    setUploadimg(texture){
        if(texture == null){
            console.log(this.config.name,this.config)
            return
        }
        this.texture = texture;
        this.uploadimg.node.active = true
        this.uploadimg.spriteFrame = new cc.SpriteFrame(texture);
        if (!(CC_JSB || cc.sys.isNative)){
            demo.Layer.addPreFabLayer('L_showimg','./L_showimg',100,function(){
                demo.Layer.sendCall('L_showimg','setUpload',this.texture)
            }.bind(this))
        }
    },

    //设置回调数据
    setCalljsData(data){
        this.calljs_data.node.active = true
        this.calljs_data.string = data
    },
        

    // update (dt) {},
});
