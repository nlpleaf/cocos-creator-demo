// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        layerFlag  : 'L_showimg',
        selfLayer  : cc.Node,
        uploadImg  : cc.Sprite,

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        demo.Layer.registCall(this);
    },

    //所有按钮的回调
    btnClick:function(event, customEventData){
        if(customEventData == "close"){//影藏大图显示
            //放大或缩小图片
            this.selfLayer.destroy()
        }
    },

    setUpload(texture){
        this.uploadImg.spriteFrame = new cc.SpriteFrame(texture);
        var width = texture.width
        var height = texture.height
        var wscale = 700/width;
        var hscale = 1100/height;
        var scale = wscale > hscale?hscale:wscale;
        console.log("scale",scale)
        console.log("this.uploadImg",this.uploadImg)
        this.uploadImg.node.scale = scale
    },

    // update (dt) {},
});
