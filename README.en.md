# cocos creator demo

#### Description
Cocos creator原生接入案列大全
在开发cococreator 应用是，经常需要去接入原生开发，这对于没有接触安卓，ios以及H5开发的人来说无疑一个绊脚石，为方便广大cocos cretor 开发者用户能方便开发自己的应用，花费了一段时间整理了一套支持安卓，ios以及H5三端都支持的底层开，但由于能力有限，有部分功能实现的不完美，请在评论区多多指教，同时也希望能帮到更多的人。
真机运行安装包下载地址：http://liu.ibijie.cn/down?randStr=9zxknt1dpz
欢迎加入技术交流群：1140497703

#### Software Architecture
基于cocos creator 2.3.4开发的demo底层

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
